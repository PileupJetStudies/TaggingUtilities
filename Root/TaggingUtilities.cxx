#include "TaggingUtilities/TaggingUtilities.h"
#include "PathResolver/PathResolver.h"
#include "xAODTruth/TruthPileupEventContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/JetAuxContainer.h"
//#include "xAODJet/JetAttributes.h"
#include "TFile.h"
#include "TF2.h"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include <fastjet/AreaDefinition.hh>
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"

namespace CP {

static SG::AuxElement::Decorator<char>  isHS("hs");
static SG::AuxElement::Decorator<char>  isPU("pu");
static SG::AuxElement::Decorator<char>  isQCD("qcd");
static SG::AuxElement::Decorator<char>  isStochastic("stochastic");
static SG::AuxElement::Decorator<float>  ptHS("hsPt");
static SG::AuxElement::Decorator<float>  ptQCD("qcdPt");
static SG::AuxElement::Decorator<int>  leadPartonHS("leadPartonHS");
static SG::AuxElement::Decorator<int>  leadPartonPU("leadPartonPU");
static SG::AuxElement::Accessor<float>  acc_jvt("Jvt");

TaggingUtilities::TaggingUtilities( const std::string& name): asg::AsgTool( name )
  {}

StatusCode TaggingUtilities::initialize(){
  m_pfotool = new CP::RetrievePFOTool("PFOtool");
  ATH_CHECK(m_pfotool->initialize());
  m_wpfotool = new CP::WeightPFOTool("WPFOtool");
  ATH_CHECK(m_wpfotool->initialize());

  const std::string name = "MyxAODAnalysis"; //string describing the current thread, for logging
  TString jetAlgo = "AntiKt4EMPFlow"; //String describing your jet collection, for example AntiKt4TopoEM or AntiKt4EMTopo (see below)
  TString config = "JES_MC15cRecommendation_PFlow_Aug2016.config"; //Path to global config used to initialize the tool (see below)
  TString calibSeq = "JetArea_Residual_EtaJES"; //String describing the calibration sequence to apply (see below)
  pfJES = new JetCalibrationTool(name);
  ATH_CHECK(pfJES->setProperty("JetCollection",jetAlgo.Data()));
  ATH_CHECK(pfJES->setProperty("ConfigFile",config.Data()));
  ATH_CHECK(pfJES->setProperty("CalibSequence",calibSeq.Data()));
  //ATH_CHECK(pfJES->setProperty("IsData",IsData?true:false));
  ATH_CHECK(pfJES->setProperty("IsData",false));
  pfJES->initializeTool(name);

  const std::string name2 = "MyxAODAnalysis2"; //string describing the current thread, for logging
  TString jetAlgo2 = "AntiKt4EMTopo"; //String describing your jet collection, for example AntiKt4TopoEM or AntiKt4EMTopo (see below)
  TString config2 = "JES_MC16Recommendation_Aug2017.config"; //Path to global config used to initialize the tool (see below)
  TString calibSeq2 = "JetArea_Residual_EtaJES_GSC"; //String describing the calibration sequence to apply (see below)
  emJES = new JetCalibrationTool(name2);
  ATH_CHECK(emJES->setProperty("JetCollection",jetAlgo2.Data()));
  ATH_CHECK(emJES->setProperty("ConfigFile",config2.Data()));
  ATH_CHECK(emJES->setProperty("CalibSequence",calibSeq2.Data()));
  //ATH_CHECK(pfJES->setProperty("IsData",IsData?true:false));
  ATH_CHECK(emJES->setProperty("IsData",false));
  emJES->initializeTool(name2);

  fjvt = new JetForwardJvtTool("fjvttool");
  ATH_CHECK(fjvt->initialize());

  pjvtag = new JetVertexTaggerTool("jvtag");
  ATH_CHECK(pjvtag->setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root"));
  ANA_CHECK(pjvtag->setProperty("VertexContainer","PrimaryVerticesNew"));
  ATH_CHECK(pjvtag->initialize());

  return StatusCode::SUCCESS;
}

StatusCode TaggingUtilities::finalize(){
  return StatusCode::SUCCESS;
}

StatusCode TaggingUtilities::buildTruthJets(){
  const xAOD::TruthParticleContainer* tpc = 0;
  ATH_CHECK(evtStore()->retrieve(tpc, "TruthParticles") ) ;
  const xAOD::TruthEventContainer* tec = 0;
  ATH_CHECK(evtStore()->retrieve(tec, "TruthEvents") ) ;
  for(const auto& tpe : *tec) {
    xAOD::JetContainer* vertjets = new xAOD::JetContainer();
    xAOD::JetAuxContainer* vertjetsAux = new xAOD::JetAuxContainer();
    vertjets->setStore(vertjetsAux);
    TString newname = "AntiKt4TruthJets";
    ATH_CHECK( evtStore()->record(vertjets,newname.Data()));
    ATH_CHECK( evtStore()->record(vertjetsAux,(newname+"Aux.").Data()));
    std::vector<fastjet::PseudoJet> input_particles;
    for (size_t i = 0; i < tpe->nTruthParticles(); i++) {
      const xAOD::TruthParticle *part = static_cast<const xAOD::TruthParticle*>(tpe->truthParticle(i));
      if (!part) continue;
      if (part->pdgId()==21 && part->e()==0) continue;
      if (abs(part->pdgId())==13) continue;
      if ((part->barcode())>2e5) continue;
      if (abs(part->pdgId())==12) continue;
      if (abs(part->pdgId())==14) continue;
      if (abs(part->pdgId())==16) continue;
      if (fabs(part->eta())>5) continue;
      fastjet::PseudoJet currconst(part->p4().Px(),part->p4().Py(),part->p4().Pz(),part->e());
      if (part->status()!=1) {
        currconst *= 1e-9;
        currconst.set_user_index(part->index());
      } else currconst.set_user_index(-1);
      input_particles.push_back(currconst);
    }
    fastjet::JetDefinition jet_def(fastjet::antikt_algorithm,0.4);
    fastjet::ClusterSequence clust_seq(input_particles,jet_def);
    std::vector<fastjet::PseudoJet> inclusive_jets = sorted_by_pt(clust_seq.inclusive_jets(10e3));
    for (size_t i = 0; i < inclusive_jets.size(); i++) {
      xAOD::Jet *jet = new xAOD::Jet;
      vertjets->push_back(jet);
      jet->setJetP4(xAOD::JetFourMom_t(inclusive_jets[i].perp(),inclusive_jets[i].rap(),inclusive_jets[i].phi(),0));
      std::vector<fastjet::PseudoJet> consts = inclusive_jets[i].constituents();
      float largestConst = 0;
      int largestPdgid = 0;
      for (size_t j = 0; j < consts.size(); j++) {
        if (consts[j].user_index()<0) continue;
        const xAOD::TruthParticle *tp = (*tpc)[consts[j].user_index()];
        if (tp->pt()<largestConst) continue;
        if (abs(tp->pdgId())>22) continue;
        largestConst = tp->pt();
        largestPdgid = tp->pdgId();
      }
      jet->auxdata<int>("PartonTruthLabelID") = largestPdgid;
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode TaggingUtilities::buildTruthPileupJets(){
  const xAOD::TruthParticleContainer* tpc = 0;
  ATH_CHECK(evtStore()->retrieve(tpc, "TruthParticles") ) ;
  const xAOD::TruthPileupEventContainer* tpec = 0;
  ATH_CHECK(evtStore()->retrieve(tpec, "TruthPileupEvents") ) ;
  for(const auto& tpe : *tpec) {
    xAOD::JetContainer* vertjets = new xAOD::JetContainer();
    xAOD::JetAuxContainer* vertjetsAux = new xAOD::JetAuxContainer();
    vertjets->setStore(vertjetsAux);
    TString newname = "AntiKt4TruthJets";
    newname += tpe->index();
    ATH_CHECK( evtStore()->record(vertjets,newname.Data()));
    ATH_CHECK( evtStore()->record(vertjetsAux,(newname+"Aux.").Data()));
    std::vector<fastjet::PseudoJet> input_particles;
    for (size_t i = 0; i < tpe->nTruthParticles(); i++) {
      const xAOD::TruthParticle *part = static_cast<const xAOD::TruthParticle*>(tpe->truthParticle(i));
      if (!part) continue;
      if (part->pdgId()==21 && part->e()==0) continue;
      if (abs(part->pdgId())==13) continue;
      if ((part->barcode())>2e5) continue;
      if (abs(part->pdgId())==12) continue;
      if (abs(part->pdgId())==14) continue;
      if (abs(part->pdgId())==16) continue;
      if (fabs(part->eta())>5) continue;
      fastjet::PseudoJet currconst(part->p4().Px(),part->p4().Py(),part->p4().Pz(),part->e());
      if (part->status()!=1) {
        currconst *= 1e-9;
        currconst.set_user_index(part->index());
      } else currconst.set_user_index(-1);
      input_particles.push_back(currconst);
    }
    fastjet::JetDefinition jet_def(fastjet::antikt_algorithm,0.4);
    fastjet::ClusterSequence clust_seq(input_particles,jet_def);
    std::vector<fastjet::PseudoJet> inclusive_jets = sorted_by_pt(clust_seq.inclusive_jets(10e3));
    for (size_t i = 0; i < inclusive_jets.size(); i++) {
      xAOD::Jet *jet = new xAOD::Jet;
      vertjets->push_back(jet);
      jet->setJetP4(xAOD::JetFourMom_t(inclusive_jets[i].perp(),inclusive_jets[i].rap(),inclusive_jets[i].phi(),0));
      std::vector<fastjet::PseudoJet> consts = inclusive_jets[i].constituents();
      float largestConst = 0;
      int largestPdgid = 0;
      for (size_t j = 0; j < consts.size(); j++) {
        if (consts[j].user_index()<0) continue;
        const xAOD::TruthParticle *tp = (*tpc)[consts[j].user_index()];
        if (tp->pt()<largestConst) continue;
        if (abs(tp->pdgId())>22) continue;
        largestConst = tp->pt();
        largestPdgid = tp->pdgId();
      }
      jet->auxdata<int>("PartonTruthLabelID") = largestPdgid;
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode TaggingUtilities::buildPFlowJets(){
  const xAOD::VertexContainer* pvertices = 0;
  ATH_CHECK( evtStore()->retrieve( pvertices, "PrimaryVertices" ) );
  const xAOD::PFOContainer* pfos = 0;
  pfos = m_pfotool->retrievePFO(CP::EM,CP::all);
  for(const auto& vx : *pvertices) {
    xAOD::JetContainer* vertjets = new xAOD::JetContainer();
    xAOD::JetAuxContainer* vertjetsAux = new xAOD::JetAuxContainer();
    vertjets->setStore(vertjetsAux);
    TString newname = "AntiKt4PFlowJets";
    newname += vx->index();
    ATH_CHECK( evtStore()->record(vertjets,newname.Data()));
    ATH_CHECK( evtStore()->record(vertjetsAux,(newname+"Aux.").Data()));

    std::vector<fastjet::PseudoJet> input_pfo;
    std::set<int> charges;
    for(const auto& pfo : *pfos) if (fabs(pfo->eta())<2.5 && pfo->charge()==0 && pfo->eEM()>0) {
      TLorentzVector corrected = pfo->GetVertexCorrectedEMFourVec(*vx);
      input_pfo.push_back(fastjet::PseudoJet(corrected.Px(),corrected.Py(),corrected.Pz(),corrected.E()));
    }
    for(const auto& pfo : *pfos) if (pfo->charge()!=0) {
      if (vx->index()==0 && fabs((vx->z()-pfo->track(0)->z0())*sin(pfo->track(0)->theta()))>2.) continue;
      if (vx->index()!=0 && vx!=pfo->track(0)->vertex()) continue;
      float pweight = 0;
      m_wpfotool->fillWeight(*pfo,pweight);
      fastjet::PseudoJet currconst(pfo->p4().Px()*pweight,pfo->p4().Py()*pweight,pfo->p4().Pz()*pweight,pfo->e()*pweight);
      currconst.set_user_index(pfo->index());
      charges.insert(pfo->index());
      input_pfo.push_back(currconst);
    }
    fastjet::JetDefinition jet_def(fastjet::antikt_algorithm,0.4);
    fastjet::AreaDefinition area_def(fastjet::active_area_explicit_ghosts,fastjet::GhostedAreaSpec(fastjet::SelectorAbsRapMax(2.5)));
    fastjet::ClusterSequenceArea clust_pfo(input_pfo,jet_def,area_def);
    std::vector<fastjet::PseudoJet> inclusive_jets = sorted_by_pt(clust_pfo.inclusive_jets(0));
    for (size_t i = 0; i < inclusive_jets.size(); i++) {
      xAOD::Jet* tempjet = new xAOD::Jet();
      vertjets->push_back(tempjet);
      xAOD::JetFourMom_t newArea(inclusive_jets[i].area_4vector().perp(),inclusive_jets[i].area_4vector().rap(),inclusive_jets[i].area_4vector().phi(),0);
      tempjet->setAttribute("ActiveArea4vec",newArea);
      xAOD::JetFourMom_t tempjetp4(inclusive_jets[i].perp(),inclusive_jets[i].rap(),inclusive_jets[i].phi(),0);
      tempjet->setJetP4(tempjetp4);
      tempjet->setJetP4("JetConstitScaleMomentum",tempjetp4);
      tempjet->setJetP4("JetPileupScaleMomentum",tempjetp4);
      TLorentzVector allcharge;
      std::vector<fastjet::PseudoJet> constituents = inclusive_jets[i].constituents();
      float chargedpart = 0;
      for (size_t j = 0; j < constituents.size(); j++) {
        if (charges.count(constituents[j].user_index())>=1) chargedpart += constituents[j].perp();
      }
      xAOD::JetFourMom_t chargejetp4(2.*chargedpart,inclusive_jets[i].rap(),inclusive_jets[i].phi(),0);
      tempjet->setJetP4("JetChargedScaleMomentum",chargejetp4);
    }
    pfJES->modify(*vertjets);
  }
  return StatusCode::SUCCESS;
}

StatusCode TaggingUtilities::chooseNewPV(int vertexNumber) {
  const xAOD::VertexContainer* pvertices = 0;
  ATH_CHECK( evtStore()->retrieve( pvertices, "PrimaryVertices" ) );
  std::pair<xAOD::VertexContainer*,xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*pvertices);
  xAOD::VertexContainer* pvertices_copy = shallowcopy.first;
  xAOD::ShallowAuxContainer* pvertices_copyAux = shallowcopy.second;
  if (vertexNumber<pvertices->size() && vertexNumber>0) {
    pvertices_copy->at(vertexNumber)->setVertexType(xAOD::VxType::VertexType::PriVtx);
    pvertices_copy->at(0)->setVertexType(xAOD::VxType::VertexType::PileUp);
  }
  TString newname = "PrimaryVerticesNew";
  ATH_CHECK( evtStore()->record(pvertices_copy,newname.Data()));
  ATH_CHECK( evtStore()->record(pvertices_copyAux,(newname+"Aux.").Data()));
  return StatusCode::SUCCESS;
}

StatusCode TaggingUtilities::buildEMTopoJets(bool allPU,TString vertexName){
  const xAOD::VertexContainer* pvertices = 0;
  ATH_CHECK( evtStore()->retrieve( pvertices, vertexName.Data() ) );
  const xAOD::JetContainer* jets = 0;
  ATH_CHECK(evtStore()->retrieve(jets, "AntiKt4EMTopoJets") ) ;
  std::pair<xAOD::JetContainer*,xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*jets);
  xAOD::JetContainer* jets_copy = shallowcopy.first;
  xAOD::ShallowAuxContainer* jets_copyAux = shallowcopy.second;
  xAOD::setOriginalObjectLink(*jets, *jets_copy);
  TString newname = "calibratedJets";
  ATH_CHECK( evtStore()->record(jets_copy,newname.Data()));
  ATH_CHECK( evtStore()->record(jets_copyAux,(newname+"Aux.").Data()));
  emJES->modify(*jets_copy);
  pjvtag->modify(*jets_copy);
  for (const auto& jet : *jets_copy) {
    //float newjvt = pjvtag->evaluateJvt(*jet);
    //float newjvt = pjvtag->updateJvt(*jet);
    if (allPU) acc_jvt(*jet) = 0;
  }
  for(const auto& vx : *pvertices) {
    xAOD::JetContainer* vertjets = new xAOD::JetContainer();
    xAOD::JetAuxContainer* vertjetsAux = new xAOD::JetAuxContainer();
    vertjets->setStore(vertjetsAux);
    TString newname = "AntiKt4EMTopoJets";
    newname += vx->index();
    ATH_CHECK( evtStore()->record(vertjets,newname.Data()));
    ATH_CHECK( evtStore()->record(vertjetsAux,(newname+"Aux.").Data()));
    for (const auto& jet : *jets_copy) {
      if (size_t(fjvt->getJetVertex(jet))!=vx->index()) continue;
      if (fabs(jet->eta())>2.5) continue;
      if ((allPU || vx->vertexType()!=xAOD::VxType::PriVtx) && !fjvt->centralJet(jet)) continue;
      if (!allPU && vx->vertexType()==xAOD::VxType::PriVtx && jet->pt()<60e3 && jet->auxdata<float>("Jvt")<0.59)  continue;
      xAOD::Jet *newJet = new xAOD::Jet();
      vertjets->push_back(newJet);
      *newJet = *jet;
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode TaggingUtilities::truthTag(const xAOD::JetContainer *jets,bool doPU) {
  if (doPU) buildTruthPileupJets();
  buildTruthJets();
  const xAOD::TruthPileupEventContainer* tpec = 0;
  if (doPU) ATH_CHECK(evtStore()->retrieve(tpec, "TruthPileupEvents") ) ;
  size_t nevents = doPU?tpec->size():0;
  const xAOD::JetContainer* truthJets = 0;
  ATH_CHECK(evtStore()->retrieve(truthJets, "AntiKt4TruthJets") ) ;
  for (const auto& jet : *jets) {
    isHS(*jet) = 0;
    isPU(*jet) = 1;
    isQCD(*jet) = 0;
    isStochastic(*jet) = 1;
    ptHS(*jet) = 0;
    ptQCD(*jet) = 0;
    leadPartonHS(*jet) = 0;
    leadPartonPU(*jet) = 0;
    for (const auto& tjet : *truthJets) if (tjet->p4().DeltaR(jet->p4())<0.6) {
      isPU(*jet) = 0;
      isStochastic(*jet) = 0;
      if (tjet->p4().DeltaR(jet->p4())>0.3) continue;
      if (tjet->pt()>ptHS(*jet)) {
        ptHS(*jet) = tjet->pt();
        //leadPartonHS(*jet) = tjet->auxdata<int>("PartonTruthLabelID");
      }
    }
    if (doPU) for (size_t evi = 0; evi < nevents; evi++) {
      const xAOD::JetContainer* truthPileupJets = 0;
      TString pileupName = "AntiKt4TruthJets";
      pileupName += evi;
      ATH_CHECK(evtStore()->retrieve(truthPileupJets,pileupName.Data()) ) ;
      for (const auto& tjet : *truthPileupJets) {
        if (tjet->p4().DeltaR(jet->p4())<0.6) isStochastic(*jet) = 0; 
        if (tjet->p4().DeltaR(jet->p4())<0.3 && tjet->pt()>ptQCD(*jet)) {
          ptQCD(*jet) = tjet->pt(); 
          leadPartonPU(*jet) = tjet->auxdata<int>("PartonTruthLabelID");
        }
      }
    }
    if (ptHS(*jet)>10e3) isHS(*jet) = 1;
    else if (isPU(*jet) && ptQCD(*jet)>10e3) isQCD(*jet) = 1;
  }
  return StatusCode::SUCCESS;  
}

float TaggingUtilities::towerWidth(const xAOD::Jet *jet,const xAOD::CaloTowerContainer *towers) {
  double towerpt = 0;
  double towerwidth = 0;
  for(const auto& tower : *towers) {
    if (tower->e()<0) continue;
    if (tower->p4().DeltaR(jet->p4())>0.4)  continue;
    towerpt += tower->pt();
    towerwidth += tower->pt()*tower->p4().DeltaR(jet->p4());
  }
  return (towerwidth/(towerpt>0?towerpt:1));
}

void TaggingUtilities::towerWidth(const xAOD::Jet *jet,const xAOD::CaloTowerContainer *towers,float &alpha,float &beta,float &gamma,int fine,float gauswidth) {
  float scale = 1.;
  if (fine==2) scale = 2.;
  if (fine==0) scale = 0.5;
  float axEtaWidth = (10./(100*scale));
  float axPhiWidth = (2*TMath::Pi()/(64*scale));
  TH2D *jetmap = new TH2D("jetmap","jetmap",1+12*scale,-6.5*axEtaWidth*scale,6.5*axEtaWidth*scale,1+12*scale,-6.5*axPhiWidth*scale,6.5*axPhiWidth*scale);
  jetmap->SetDirectory(0);
  TLorentzVector VJ;
  VJ.SetPtEtaPhiM(jet->pt(),jet->auxdata<float>("DetectorEta"),jet->phi(),0);
  for(const auto& tower : *towers) {
    if (tower->e()<=0 || tower->p4().DeltaR(jet->p4())>2)  continue;
    TLorentzVector VT;
    VT.SetPtEtaPhiM(tower->pt(),tower->eta(),tower->phi(),0);
    if (fabs(VT.DeltaPhi(VJ))<6.1*axPhiWidth*scale && fabs(VT.Eta()-VJ.Eta())<6.1*axEtaWidth*scale) jetmap->Fill(VJ.Eta()-VT.Eta(),fabs(VJ.DeltaPhi(VT)),tower->pt()/2);
    if (fabs(VT.DeltaPhi(VJ))<6.1*axPhiWidth*scale && fabs(VT.Eta()-VJ.Eta())<6.1*axEtaWidth*scale) jetmap->Fill(VJ.Eta()-VT.Eta(),-1*fabs(VJ.DeltaPhi(VT)),tower->pt()/2);
  }
  TString stringwidth = "";
  stringwidth += gauswidth;
  TString formula = "[1]+x*[2]+[0]*TMath::Gaus(x,0,WIDTH)*TMath::Gaus(y,0,WIDTH)";
  formula.ReplaceAll("WIDTH",stringwidth);
  TF2 *f1 = new TF2("f1",formula.Data(),-5.5*axEtaWidth*scale,5.5*axEtaWidth*scale,-5.5*axPhiWidth*scale,5.5*axPhiWidth*scale);
  jetmap->Fit("f1","QO W R");
  TF2 *fit = (TF2*)jetmap->GetFunction("f1");
  gamma = fit?fit->GetParameter(0):0;
  alpha  = fit?fit->GetParameter(1):0;
  beta = fit?fit->GetParameter(2):0;
  delete jetmap;
  delete f1;
}

float TaggingUtilities::towerWidth(const xAOD::Jet *jet,const xAOD::CaloClusterContainer *towers) {
  double towerpt = 0;
  double towerwidth = 0;
  for(const auto& tower : *towers) {
    if (tower->e()<0) continue;
    if (tower->p4().DeltaR(jet->p4())>0.4)  continue;
    towerpt += tower->pt();
    towerwidth += tower->pt()*tower->p4().DeltaR(jet->p4());
  }
  return (towerwidth/(towerpt>0?towerpt:1));
}

void TaggingUtilities::towerWidth(const xAOD::Jet *jet,const xAOD::CaloClusterContainer *towers,float &alpha,float &beta,float &gamma,int fine,float gauswidth) {
  float scale = 1.;
  if (fine==2) scale = 2.;
  if (fine==0) scale = 0.5;
  float axEtaWidth = (10./(100*scale));
  float axPhiWidth = (2*TMath::Pi()/(64*scale));
  TH2D *jetmap = new TH2D("jetmap","jetmap",1+12*scale,-6.5*axEtaWidth*scale,6.5*axEtaWidth*scale,1+12*scale,-6.5*axPhiWidth*scale,6.5*axPhiWidth*scale);
  jetmap->SetDirectory(0);
  TLorentzVector VJ;
  VJ.SetPtEtaPhiM(jet->pt(),jet->auxdata<float>("DetectorEta"),jet->phi(),0);
  for(const auto& tower : *towers) {
    if (tower->e()<=0 || tower->p4().DeltaR(jet->p4())>2)  continue;
    TLorentzVector VT;
    VT.SetPtEtaPhiM(tower->pt(),tower->eta(),tower->phi(),0);
    if (fabs(VT.DeltaPhi(VJ))<6.1*axPhiWidth*scale && fabs(VT.Eta()-VJ.Eta())<6.1*axEtaWidth*scale) jetmap->Fill(VJ.Eta()-VT.Eta(),fabs(VJ.DeltaPhi(VT)),tower->pt()/2);
    if (fabs(VT.DeltaPhi(VJ))<6.1*axPhiWidth*scale && fabs(VT.Eta()-VJ.Eta())<6.1*axEtaWidth*scale) jetmap->Fill(VJ.Eta()-VT.Eta(),-1*fabs(VJ.DeltaPhi(VT)),tower->pt()/2);
  }
  TString stringwidth = "";
  stringwidth += gauswidth;
  TString formula = "[1]+x*[2]+[0]*TMath::Gaus(x,0,WIDTH)*TMath::Gaus(y,0,WIDTH)";
  formula.ReplaceAll("WIDTH",stringwidth);
  TF2 *f1 = new TF2("f1",formula.Data(),-5.5*axEtaWidth*scale,5.5*axEtaWidth*scale,-5.5*axPhiWidth*scale,5.5*axPhiWidth*scale);
  jetmap->Fit("f1","QO W R");
  TF2 *fit = (TF2*)jetmap->GetFunction("f1");
  gamma = fit?fit->GetParameter(0):0;
  alpha  = fit?fit->GetParameter(1):0;
  beta = fit?fit->GetParameter(2):0;
  delete jetmap;
  delete f1;
}

} /* namespace CP */
