
#include "GaudiKernel/DeclareFactoryEntries.h"

// Local include(s):
#include "TaggingUtilities/TaggingUtilities.h"

DECLARE_NAMESPACE_TOOL_FACTORY( CP, TaggingUtilities )

DECLARE_FACTORY_ENTRIES( TaggingUtilities ) {

   DECLARE_NAMESPACE_TOOL( CP, TaggingUtilities )

}
