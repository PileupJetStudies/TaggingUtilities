#ifndef TAGGINGUTILITIESSCALEFACTORS_H_
#define TAGGINGUTILITIESSCALEFACTORS_H_

#include "PATInterfaces/SystematicsTool.h"
#include "AsgTools/AsgTool.h"
#include "TRandom3.h"
#include "TLorentzVector.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "PFlowUtils/RetrievePFOTool.h"
#include "PFlowUtils/WeightPFOTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetMomentTools/JetForwardJvtTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "xAODCaloEvent/CaloTowerContainer.h"

#include <TH2.h>
#include <string>

namespace CP {

class TaggingUtilities: public asg::AsgTool {
    ASG_TOOL_CLASS( TaggingUtilities,asg::IAsgTool)

public:
    TaggingUtilities( const std::string& name);

    virtual StatusCode initialize();
    virtual StatusCode finalize();

    //Build truth pileup jets, requiring pt>10 GeV. 
    //Structured as vector of jet containers, where each container corresponds to a truth pileup event.
    StatusCode buildTruthPileupJets();
    StatusCode buildTruthJets();
    //Build pflow jets. 
    //Structured as vector of jet containers, where each container corresponds to a vertex.
    //Does active area calculation, and runs jet finding for every vertex, so this is expensive.
    //In the future, will apply cuts on vertices before running jet finding and perhaps will 
    //do faster area calculations.
    StatusCode buildPFlowJets();
    //Group EMTopo jets. 
    //Structured as vector of jet containers, where each container corresponds to a vertex.
    StatusCode buildEMTopoJets(bool allPU=false,TString vertexName="PrimaryVertices");

    StatusCode chooseNewPV(int vertexNumber);

    //Do truth tagging. 
    //If doPU is false, tagging does not use truth pileup jets.
    //If doPU is true, required user to first call buildTruthPileupJets
    StatusCode truthTag(const xAOD::JetContainer *jets,bool doPU=true);
    
    float towerWidth(const xAOD::Jet *jet,const xAOD::CaloTowerContainer *towers);
    void towerWidth(const xAOD::Jet *jet,const xAOD::CaloTowerContainer *towers,float &alpha,float &beta,float &gamma,int fine=1,float gauswidth=0.1);
    float towerWidth(const xAOD::Jet *jet,const xAOD::CaloClusterContainer *towers);
    void towerWidth(const xAOD::Jet *jet,const xAOD::CaloClusterContainer *towers,float &alpha,float &beta,float &gamma,int fine=1,float gauswidth=0.1);

private:

    TaggingUtilities();

    JetCalibrationTool *pfJES;//!
    JetCalibrationTool *emJES;//!
    CP::IRetrievePFOTool *m_pfotool;//!
    CP::IWeightPFOTool *m_wpfotool;//!
    JetForwardJvtTool *fjvt;//!
    JetVertexTaggerTool* pjvtag;//!

};

} /* namespace CP */

#endif /* TAGGINGUTILITIESSCALEFACTORS_H_ */
