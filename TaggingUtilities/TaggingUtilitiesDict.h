#ifndef TAGGINGUTILITIESDICT_H_
#define TAGGINGUTILITIESDICT_H_

#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

#include "TaggingUtilities/TaggingUtilities.h"

#endif /* TAGGINGUTILITIESDICT_H_ */
